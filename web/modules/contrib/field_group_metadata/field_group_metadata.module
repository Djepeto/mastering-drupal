<?php

/**
 * @file
 * Code for the Field Group Metadata module.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_help().
 */
function field_group_metadata_help($route_name, \Drupal\Core\Routing\RouteMatchInterface $route_match) {
  if ($route_name == 'help.page.field_group_metadata') {
    $filepath = dirname(__FILE__) . '/README.txt';
    if (file_exists($filepath)) {
      $readme = file_get_contents($filepath);
    }
    if (!isset($readme)) {
      return NULL;
    }
    $moduleHandler = \Drupal::moduleHandler();
    if ($moduleHandler->moduleExists('markdown')) {
      $filters = $moduleHandler->invoke('markdown', 'filter_info');
      $info = $filters['filter_markdown'];

      if (function_exists($info['process callback'])) {
        $output = $info['process callback']($readme, NULL);
      }
      else {
        $output = '<pre>' . $readme . '</pre>';
      }
    }
    else {
      $output = '<pre>' . $readme . '</pre>';
    }

    return $output;
  }
  return '';
}

/**
 * Alter the form to add the preprocessing routine.
 *
 * Implements hook_form_alter().
 */
function field_group_metadata_form_alter(&$form, FormStateInterface $form_state, $form_id) {

  // Firstly check if the module field_group is enabled.
  $moduleHandler = \Drupal::service('module_handler');
  if (!$moduleHandler->moduleExists('field_group')){
    \Drupal::service('messenger')->addError(
      t('The module "field_group" is required but not installed!<br>Please install it to use the field_group_metadata functionality.')
    );
    return;
  }

  // Add a pre render function to all forms with a group_metadata element.
  // This way we can easily alter all forms and move the metadata group to the
  // advanced section of the form.
  if (isset($form['#fieldgroups'])
    && isset($form['#fieldgroups']['group_metadata'])
    && !empty($form['#fieldgroups']['group_metadata'])) {
    $form['#pre_render'] = [
      ['\\Drupal\\field_group_metadata\\FieldGroupMetadataPreRenderer', 'preRender'],
    ];
  }
}
