CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers

INTRODUCTION
------------

This module allows you to create a metadata field group, provided by the field
group module, which can hold all your meta information about a content. This
group will then automatically be moved to the advanced section of your content
edit form, leaving the way open to view your primary content only, and having
the meta information out of the way, but still prominently concentrated.

* For a full description of the module, visit the project page:
https://www.drupal.org/project/field_group_metadata

* To submit bug reports and feature suggestions, or to track changes:
https://www.drupal.org/project/issues/field_group_metadata

REQUIREMENTS
------------

This module requires the following modules:

 * Field Group (https://www.drupal.org/project/field_group)

RECOMMENDED MODULES
-------------------

 * Markdown filter (https://www.drupal.org/project/markdown):
   When enabled, display of the project's README.md help will be rendered
   with markdown.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

This module has no administration interface. When uninstalling the module, the
metadata groups will just be displayed as usual in the main content part of
the edit form. No particular data can be lost.

TROUBLESHOOTING
---------------

 * My group does not get moved to the right!

   - Ensure that the technical name of your group is "group_metadata". Other
     field groups won't be moved.

MAINTAINERS
-----------

Current maintainers:
 * Florian Müller (fmueller_previon) - https://www.drupal.org/user/3524567

This project has been sponsored by:
 * Previon Plus AG
   Previon+ is specialized in digital services & platforms and supports
   its clients on finding suitable ideas, developing digital business models
   and processes and accompanies them on their digital experience.
   Visit https://www.previon.ch for more information.
