<?php

namespace Drupal\field_group_metadata;

use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Class FieldGroupMetadataPreRenderer
 *
 * @package Drupal\field_group_metadata
 */
class FieldGroupMetadataPreRenderer implements TrustedCallbackInterface {

  /**
   * {@inheritDoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * Move group metadata into the advanced section of the form.
   */
  public static function preRender($element) {
    if (!empty($element['group_metadata'])) {
      $element['advanced']['group_metadata'] = $element['group_metadata'];
      $element['advanced']['group_metadata']['#weight'] = -1000;
      // Unset the original metadata group.
      unset($element['group_metadata']);
    }

    // Copy save/preview buttons to the advanced section.
    if (!empty($element['actions'])) {
      $element['advanced']['actions'] = $element['actions'];
      $element['advanced']['actions']['#weight'] = 1000;
    }

    return $element;
  }

}
